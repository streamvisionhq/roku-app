
Sub main()   
  generalSettings = CreateObject("roRegistrySection", "GenSettings")    
    deviceInfo = CreateObject("roDeviceInfo")
    deviceID = deviceInfo.GetDeviceUniqueId()
    m.deviceID = deviceID
    m.recordingStarted = false
    m.recording_title = ""
    m.screen = {}
    m.captionIndex = 0
    m.noDeviceDeleted = false
    m.callMain = true
    m.exitDelete = false
    m.isRecording = 0    
    m.exitIndex = 0
    m.captionIndex = 0
    m.logoOnce = true
    m.epgFlag=true
    m.epgUpDownFlag=false
    m.deviceInfoData = deviceInfo.GetDisplaySize()
    m.initalLoadIndex=0   
    m.initialLoad=true
    m.totalChannels = {}
    m.selectedChannel=0
    m.tempSelectedChannel=0
    m.exit="YES"
    m.DeleteSuccess = false
    m.loadingMsgX=0
    m.loadingMsg=""
    m.timeInterval=10000 * 10 * 60
    m.clock = CreateObject("roTimespan")
    m.next_call = m.clock.TotalMilliseconds() + m.timeInterval 
    m.errorCMSMsg = "Server Error"
    m.errorCMSCode = "2001"
    m.errorSPMsg = "Server Error"
    m.errorSPCode = "1001"
    m.onlyOnce = true
    m.channelLogoRepo=[]
    m.allowOK=false 
    m.logoY = 0
    m.player = {}
    m.drawBufferProgressBar = drawVideoBufferProgressBar
    m.textFontRegistry = CreateObject("roFontRegistry")
    m.fontRegistry = CreateObject("roFontRegistry")
    m.textFontRegistry.Register("file://pkg:/assets/fonts/Roboto/Roboto-Medium.ttf")
    m.channelFetched = true
    m.playVideoCalled = 0
    m.loadingPercentFont = m.fontRegistry.getFont("Roboto Medium" ,20, true, false)
    m.loadingTextFont = m.fontRegistry.getFont("Roboto Medium" ,40, true, false)
    m.loadingFont = m.fontRegistry.getFont("Roboto Medium" ,15, true, false)
    m.ChannelLogo = m.fontRegistry.getFont("Roboto Medium" ,25, true, false)
    m.ErrorMsg = m.fontRegistry.getFont("Roboto Medium" ,20, true, false)
    m.ErrorMsg2 = m.fontRegistry.getFont("Roboto Medium" ,30, true, false)
    m.secondsFromTime = videoScreenSecondsFromTime
    m.TimeFromSeconds = videoScreenTimeFromSeconds
    m.elapsedSeconds = 0
    m.epgBitmapX = 0
    m.epgBitmapY = 0	
    m.epgChnlImageX=35
    m.epgChnlImageY=8
    m.epgBitmapWidth = 400
    m.epgBitmapHeight = 720
    m.chnlDivLineX=0
    m.chnlDivLineY=78
    m.chnlDivLineHeight=2
    m.chnlDivLineWidth=400
    m.chnlSelctBoxX=0
    m.chnlSelctBoxY=318
    m.chnlSelctBoxHeight=82
    m.chnlSelctBoxWidth=400
    m.chnlSelctBoxInnerX=2
    m.chnlSelctBoxInnerY=320
    m.chnlSelctBoxInnerHeight=78
    m.chnlSelctBoxInnerWidth=396
    m.chnlCount2 = 0
    m.chnlCount=0
    m.bufferProgress = 0
    m.selectedVideoURL=""
    m.drawSettings = {
		showInfo:false,
		elapsedTime: 0
	}
    m.showLoadingMsg = true
    m.showLogo = true
    m.screenSize = {}
    m.screenSize.width = 0
    m.screenSize.height = 0
    if generalSettings.Exists("cmsURL")
        if generalSettings.Exists("CustomerID")
           
          
            serviceProviderID = generalSettings.Read("ServiceProviderID")
            cmsURL = generalSettings.Read("cmsURL")
            customerID = generalSettings.Read("CustomerID")
            password = generalSettings.Read("Password")
           
            controlResult=controlAuthentication(serviceProviderID, true)
            if controlResult<>invalid then  
               
                if controlResult.success = "false" then
                 
                    errorMessage(controlResult.message, controlResult.error_code, true)
                else
                    m.spModules = controlResult.modules
                    authenticationResult=cmsAuthentication(cmsURL, deviceID, customerID, password)
                    if authenticationResult <> invalid                          
                      if authenticationResult.success = false then
                           
                                errorMessage(authenticationResult.message, authenticationResult.error_code, true)
                            
                    else
                                if authenticationResult.data.delete = true then
                                    
                                    if (m.noDeviceDeleted = false) then                                                                      
                                        showDeleteDevice(authenticationResult.data.devices, authenticationResult.data.total_services, authenticationResult.data.total_devices, cmsURL, customerID, password, true)                                                                                              
                                    end if
                                else                 
                                    m.screen = CreateObject("roScreen")
                                    m.screenSize.width = m.screen.GetWidth()
                                    m.screenSize.height = m.screen.GetHeight()
                                    m.loadingMsg = "Loading Channel List..."
                                    m.loadingMsgX=(m.deviceInfoData.w-(len(m.loadingMsg)*20))\2
                                    m.loadingMsgY = m.screenSize.height * 0.4
                                    m.screen.DrawText(m.loadingMsg, m.loadingMsgX, m.loadingMsgY, &hFFFFFFFF, m.loadingTextFont)
                                    m.screen.SwapBuffers()
                                    m.customerModules = authenticationResult.data.modules
                                    m.customerBaseServices = authenticationResult.data.roku_service
                                    getCurrentShow()
                                    m.totalChannels = getChannels(cmsURL)
                                    loadChannelImages(m.totalChannels) 
                                    getFirstStream(m.totalChannels)                            
                                end if        
                                                
                           

                        End if
                    else
                        errorMessage(m.errorCMSMsg, m.errorCMSCode, true)
                    End if  
                End if
             else
                errorMessage(m.errorSPMsg, m.errorSPCode, true)
            End if
        else
            customerIDScreen()
        End if
    else
        addServiceProviderIDScreen()
    End if
End Sub

Function controlAuthentication(serviceProviderID, authenticationOnly)
    authenticationRequest = CreateObject("roUrlTransfer")
    if authenticationOnly = true
        authenticationRequest.SetURL("http://control.streamvisiontv.com/api/web/serviceproviders/detail/"+serviceProviderID+"?key=K72kWVncihXmijxn2EAOlpabKu6-NGee&action=authentication")
    else
        authenticationRequest.SetURL("http://control.streamvisiontv.com/api/web/serviceproviders/detail/"+serviceProviderID+"?key=K72kWVncihXmijxn2EAOlpabKu6-NGee")
    end if
    response = authenticationRequest.GetToString()

    if response = ""
        responseAuthentication = { success : "false", error_code : "1002", message : "Server Error" }
    else
        responseAuthentication = ParseJson(response)
    endif    
    
    if responseAuthentication<>invalid then    
        if responseAuthentication.success = "false" then
            controlStagingResult = controlStagingAuthentication(serviceProviderID, authenticationOnly)
            return controlStagingResult
        else
            return responseAuthentication
        end if
    else
        controlStagingResult = controlStagingAuthentication(serviceProviderID, authenticationOnly)
        return controlStagingResult
    endif

End Function

Function controlStagingAuthentication(serviceProviderID, authenticationOnly)
    authenticationRequest = CreateObject("roUrlTransfer")
    if authenticationOnly = true
        authenticationRequest.SetURL("http://streamvision.coduplabs.com/api/web/serviceproviders/detail/"+serviceProviderID+"?key=K72kWVncihXmijxn2EAOlpabKu6-NGee&action=authentication")
    else
        authenticationRequest.SetURL("http://streamvision.coduplabs.com/api/web/serviceproviders/detail/"+serviceProviderID+"?key=K72kWVncihXmijxn2EAOlpabKu6-NGee")
    end if    
    responseAuthentication = ParseJson(authenticationRequest.GetToString())          
    return responseAuthentication     
End Function

Function cmsAuthentication(cmsURL, deviceID, customerID, password) 
    deviceInfo = CreateObject("roDeviceInfo")
    deviceName = deviceInfo.GetFriendlyName()
    deviceIdentifier = deviceName.Replace(" ", "_")
    deviceID = deviceInfo.GetDeviceUniqueId()
    cmsAuthenticationURL = cmsURL+"api/device/"+customerID+"/"+password+"/"+deviceID+"/"+deviceIdentifier+"/account"       
    cmsAuthenticationObject = CreateObject("roUrlTransfer")
    cmsAuthenticationObject.setUrl(cmsAuthenticationURL)
    cmsAuthenticationResponse = cmsAuthenticationObject.getToString()
    activationResponse=ParseJson(cmsAuthenticationResponse)
   
    return activationResponse
End Function

Function addServiceProviderIDScreen() As Void
     sec = CreateObject("roRegistrySection", "GenSettings")
     screen = CreateObject("roKeyboardScreen")
     port = CreateObject("roMessagePort")
     screen.SetMessagePort(port)
     screenText = "Service Provider ID"
     screen.SetTitle(screenText)
     if sec.Exists("ServiceProviderID")
        
        screen.SetText(sec.Read("ServiceProviderID"))
     else
        screen.SetText("")
     end if
     screen.SetDisplayText("enter service provider id")
     screen.SetMaxLength(10)
     screen.AddButton(1, "Next")
     if sec.Exists("ServiceProviderID")
        screen.AddButton(2, "Cancel")
     end if
     screen.Show()
  
     while true
        msg = wait(0, screen.GetMessagePort())    
        
       
    
         if type(msg) = "roKeyboardScreenEvent"
             if msg.isScreenClosed()
                 return
             else if msg.isButtonPressed() then                 
                if msg.GetIndex() = 1
                    if screen.GetText() = "" then
                       error_message = "Please provide service provider id"
                       blankFieldMessages(error_message)
                       addServiceProviderIDScreen()
                    else 
                        screenText = screenText+" (Loading ...)"
                        screen.SetTitle(screenText)
                        serviceProviderID = screen.GetText()
                        controlResult=controlAuthentication(serviceProviderID, false)                    
                        if controlResult<>invalid then            
                            if controlResult.success = "false" then
                                screen.SetTitle(screenText)
                                errorMessage(controlResult.message, controlResult.error_code, false)
                                addServiceProviderIDScreen()
                            else
                                cmsURL = controlResult.ServiceProvider.cmsURL
                                heading = controlResult.ServiceProvider.heading
                                logo = controlResult.ServiceProvider.logo                            
                                sec.Delete("ServiceProviderID")
                                sec.Delete("cmsURL")
                                sec.Delete("heading")
                                sec.Delete("logo")
                                sec.Write("ServiceProviderID", serviceProviderID)
                                sec.Write("cmsURL", cmsURL)
                                sec.Write("heading", heading)
                                sec.Write("logo", logo)                            
                                sec.Flush()
                                screenText = "Service Provider ID"
                                customerIDScreen()
                                if m.exitDelete = true then                                    
                                    exit while
                                end if
                            end if
                        else
                            screen.SetTitle(screenText)
                            errorMessage(m.errorSPMsg, m.errorSPCode, false)
                            addServiceProviderIDScreen()
                        end if   
                    end if
                else if msg.GetIndex() = 2 then 
                    
                    if sec.Read("lastPlayed") <> "" then
                        getFirstStream(m.totalChannels)
                    else
                        screen.Close()                       
                    end if   
                    sec.Flush()
                endif
             endif
         endif
     end while
End Function

Function customerIDScreen() As Void 
    generalSettings = CreateObject("roRegistrySection", "GenSettings")
   
    screen = CreateObject("roKeyboardScreen")
    port = CreateObject("roMessagePort")
    screen.SetMessagePort(port)
    screenText = "Customer ID"
    screen.SetTitle(screenText)
   
    if generalSettings.Exists("CustomerID")
       screen.SetText(generalSettings.Read("CustomerID"))
    else
       screen.SetText("")
    end if
    screen.SetDisplayText("enter customer id")
    screen.SetMaxLength(10)
    screen.AddButton(1, "Next")
    if generalSettings.Exists("CustomerID")
       screen.AddButton(2, "Cancel")
    end if
    screen.Show()

    while true
        msg = wait(0, screen.GetMessagePort()) 
       
        if msg.GetIndex() = 0                   
            addServiceProviderIDScreen()
        else if type(msg) = "roKeyboardScreenEvent"           
            if msg.isScreenClosed()
                return
            else if msg.isButtonPressed() then
                 
                if msg.GetIndex() = 1
                    if screen.GetText() = "" then
                        error_message = "Please enter customer id"
                        blankFieldMessages(error_message)
                        customerIDScreen()
                    else 
                        screen.SetTitle(screenText+" (Loading ...)")
                        customerID = screen.GetText()
                        passwordScreen(customerID)
                        if m.exitDelete = true then                                    
                            exit while
                        end if
                    end if
               else if (msg.GetIndex() = 2) then                    
                   getFirstStream(m.totalChannels)
               endif
            endif
        endif
    end while
   
   
End Function
Function passwordScreen(customerID) As Void 
    generalSettings = CreateObject("roRegistrySection", "GenSettings")
    screen = CreateObject("roKeyboardScreen")
    port = CreateObject("roMessagePort")
    screen.SetMessagePort(port)
    screenText = "Password"
    screen.SetTitle(screenText)
    if generalSettings.Exists("Password")
       screen.SetText(generalSettings.Read("Password"))
    else
       screen.SetText("")
    end if
    screen.SetDisplayText("enter password")
    screen.SetMaxLength(10)
    screen.AddButton(1, "Save")
    if generalSettings.Exists("Password")
       screen.AddButton(2, "Cancel")
    end if
    screen.Show()

    while true
        msg = wait(0, screen.GetMessagePort())         
        if type(msg) = "roKeyboardScreenEvent"
            if msg.isScreenClosed()
                return
            else if msg.isButtonPressed() then                 
               if msg.GetIndex() = 1
                    if screen.GetText() = "" then
                        error_message = "Please enter password"
                        blankFieldMessages(error_message)
                        passwordScreen(customerID)
                    else 
                        screen.SetTitle(screenText+" (Loading ...)")
                        password = screen.GetText()
                        cmsURL = generalSettings.Read("cmsURL")
                        deviceInfo = CreateObject("roDeviceInfo")
                        deviceID = deviceInfo.GetDeviceUniqueId()
                        authenticationResult=cmsAuthentication(cmsURL, deviceID, customerID, password)
                        
                        if authenticationResult <> invalid
                            if authenticationResult.success = false then
                                screen.SetTitle(screenText)
                                
                                errorMessage(authenticationResult.message, authenticationResult.error_code, false)
                               
                                addServiceProviderIDScreen()
                            else
                                if authenticationResult.data.delete = true then
                                    generalSettings.Delete("CustomerID")
                                    generalSettings.Delete("Password")
                                    generalSettings.Write("CustomerID", customerID)
                                    generalSettings.Write("Password", password)
                                    generalSettings.Delete("secondLastPlayed")
                                    generalSettings.Delete("lastPlayed")
                                    generalSettings.Delete("secondLastSVID")
                                    generalSettings.Delete("lastSVID")
                                    generalSettings.Delete("secondLastSelectedChannel")
                                    generalSettings.Delete("lastSelectedChannel")
                                    generalSettings.Delete("secondLastPlayedMsg")
                                    generalSettings.Delete("lastPlayedMsg")                                  
                                    showDeleteDevice(authenticationResult.data.devices, authenticationResult.data.total_services, authenticationResult.data.total_devices, cmsURL, customerID, password, false)
                                else
                                    generalSettings.Delete("CustomerID")
                                    generalSettings.Delete("Password")
                                    generalSettings.Write("CustomerID", customerID)
                                    generalSettings.Write("Password", password)
                                    generalSettings.Delete("secondLastPlayed")
                                    generalSettings.Delete("lastPlayed")
                                    generalSettings.Delete("secondLastSVID")
                                    generalSettings.Delete("lastSVID")
                                    generalSettings.Delete("secondLastSelectedChannel")
                                    generalSettings.Delete("lastSelectedChannel")
                                    generalSettings.Delete("secondLastPlayedMsg")
                                    generalSettings.Delete("lastPlayedMsg")
                                    main()
                                end if                            
                            end if
                        else
                            screen.SetTitle(screenText)
                            errorMessage(m.errorCMSMsg, m.errorCMSCode, false)
                            addServiceProviderIDScreen()
                        end if
                    end if
               else if (msg.GetIndex() = 2) then                    
                   getFirstStream(m.totalChannels)
               endif
            endif
        endif
    end while
End Function
Function showDeleteDevice(devices, totalRokuServices, totalRokuDevices, cmsURL, customerID, password, restartAPP)    
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
    dialog.SetTitle("Delete Device")    
    dialog.SetText("Delete any one Roku device from below list")    
    dialog.SetText("Total Roku Devices: "+totalRokuDevices)
    dialog.SetText("Total Roku Services: "+totalRokuServices)
            
    for i=0 to devices.count()-1 step 1
        dialog.addButton(i, devices[i].Identifier + " - " + devices[i].DeviceID)
    end for    
    dialog.AddButtonSeparator()
    dialog.addButton(i+1, "Change Account Settings")
    dialog.addButton(i+2, "Exit")
    m.exitIndex = i+2

    dialog.EnableBackButton(false)
    dialog.Show()

    While True
        dlgMsg = wait(0, dialog.GetMessagePort())
        If type(dlgMsg) = "roMessageDialogEvent"
            if (dlgMsg.isButtonPressed()) then  
                if (dlgMsg.GetIndex() = m.exitIndex) then
                    m.exitDelete = true
                    exit while
                else if (dlgMsg.GetIndex() = m.exitIndex-1)
                    addServiceProviderIDScreen()
                    dialog.Close()
                else
                    deviceIndex = dlgMsg.GetIndex()
                    deleteDeviceID = devices[deviceIndex].DeviceID
                    dialog.ShowBusyAnimation()
                    
                    deleteResponse = deleteDevice(deleteDeviceID, cmsURL, customerID, password)
                    if deleteResponse <> invalid                
                        if deleteResponse.success = true then
                            if totalRokuDevices.ToInt() > totalRokuServices.ToInt()
                                difference = totalRokuDevices.ToInt() - totalRokuServices.ToInt()                                
                                if difference > 1                                    
                                    m.exitDelete = true
                                    exit while
                                else                                    
                                    m.DeleteSuccess = true
                                    main()
                                end if                                
                            else
                                m.DeleteSuccess = true
                                main()
                            end if
                        end if
                    end if
                end if
            else if (dlgMsg.isScreenClosed()) then                
                exit while               
            else
                exit while                
            end if
        end if
    end while
    
End Function

Function deleteDevice(deviceID, cmsURL, customerID, password)    
  
    deleteDeviceURL = cmsURL+"api/device/"+customerID+"/"+password+"/"+deviceID+"/delete"
    deleteDeviceObject = CreateApiURLTransferObject(deleteDeviceURL)
    deleteDeviceResponse = deleteDeviceObject.getToString()
    deleteResponse=ParseJson(deleteDeviceResponse)
    return deleteResponse
End Function



Function getChannels(cmsURL As String)
   
    channelsRequest = CreateObject("roUrlTransfer")
    channelsRequest.SetURL(cmsURL+"api/epg/channels")
    channelsResponse = ParseJson(channelsRequest.GetToString())
    
    if channelsResponse<>invalid then
        if channelsResponse.success = false then
            errorMessage(channelsResponse.message, channelsResponse.error_code)
        else
            m.totalChannels=channelsResponse
            return m.totalChannels
        end if      
    else
            errorMessage(m.errorCMSMsg, m.errorCMSCode, true)        
    end if
End Function

Function loadChannelImages(totalChannels as object)
    
  
   
    print "Function Init"
    if m.onlyOnce=true then
            m.onlyOnce=false
            for q=0 to totalChannels.data.Count()-1 step 1
                print q
                xfer = CreateApiURLTransferObject(totalChannels.data[q].channelLogo)
                tmppath = "tmp:/ThumbNailImage.jpg"
                xfer.gettoFile(tmppath)
                chnlLogo=CreateObject("roBitmap",tmppath)
                tmppath = "tmp:/ThumbNailImage.jpg"
                m.channelLogoRepo.push(chnlLogo)
            end for
    end if
    
End Function

function errorTop(message as string)
    canvasError=CreateObject("roBitmap", {width:340, height:140, AlphaEnable: true})
    canvasError.Clear(&h404040A0)
    m.logoY = m.screenSize.height * 0.03
    canvasError.DrawRect(0,0,300,300,&h0000FF00)
    canvasError.DrawText(message,15,15,&hFFFFFFFF,m.channelLogo)
    if m.isRecording = 0
        canvasError.DrawScaledObject(15,50,1,1,m.channelLogoRepo[m.selectedChannel])
        canvasError.DrawText(m.totalChannels.data[m.selectedChannel].title,140,55,&hFFFFFFFF,m.channelLogo)
    end if     
    m.screen.DrawObject((m.screenSize.width)-((m.screenSize.width) * 0.25),15,canvasError)
    m.screen.SwapBuffers()
    sleep(3000)
End function

function getFirstStream(totalChannels as object, error="") As Void
    if error <> "" then
        errorTop(error)
    end if
    
    m.isRecording = 0
    sec = CreateObject("roRegistrySection", "GenSettings")
    if sec.Read("lastPlayed") <> "" then
    print "####################FOR ANDROID APP TEST##########"
        print sec.Read("lastPlayed")
        print "####################FOR ANDROID APP TEST##########"
        m.initialLoad=false
        m.loadingMsg=sec.Read("lastPlayedMsg")
        
        m.selectedChannel=sec.Read("lastSelectedChannel").ToInt()       
        m.tempSelectedChannel = m.selectedChannel     
        svChannelId = sec.Read("lastSVID")     
        
        fetchedUrl=getStreamUrl(svChannelId)      
        if fetchedUrl = "" 
            while fetchedUrl = ""
                fetchedUrl = updateToNextChannel()
            end while
        end if      
        playVideo(fetchedUrl)
    else
        m.initialLoad=true
        if m.initalLoadIndex=totalChannels.data.count()-1 then
            m.initalLoadIndex=0
        end if
        for x=m.initalLoadIndex to totalChannels.data.count()-1 step 1
            m.initalLoadIndex=m.initalLoadIndex+1
            firststream=getStreamUrl(totalChannels.data[x].svChannelId.toStr())
            m.loadingMsg="Loading "+totalChannels.data[x].description+" ..."
            
            if firststream <> "" and firststream <> "exit app" then
                m.selectedChannel=x
                m.tempSelectedChannel = m.selectedChannel
                m.initialLoad=false
                exit for
            else 
                if firststream = "exit app"
                    exit for
                end if
                if firststream = "" 
                    while firststream = ""
                        firststream = updateToNextChannel()
                    end while
                end if
            end if
        end for
        if firststream <> "" and firststream <> "exit app"
            selectedChannelString = m.selectedChannel.ToStr()
            sec.Write("lastSelectedChannel", selectedChannelString)
            sec.Write("lastPlayed", firststream)            
            sec.Write("lastPlayedMsg", m.loadingMsg)

            SVID = totalChannels.data[m.selectedChannel].svChannelId.toStr()       
            sec.Write("lastSVID", SVID)

            playVideo(firststream)
        end if
    end if
end function

function channelInfo()
    canvas=CreateObject("roBitmap", {width:520, height:140, AlphaEnable: true})
    canvas.Clear(&h404040A0)
    m.logoY = m.screenSize.height * 0.03
    canvas.DrawScaledObject(15,15,1,1,m.channelLogoRepo[m.selectedChannel])
    canvas.DrawText(m.totalChannels.data[m.selectedChannel].title,140,20,&hFFFFFFFF,m.channelLogo)
    canvas.DrawText(m.totalChannels.data[m.selectedChannel].description,15,100,&hFFFFFFFF,m.channelLogo)
    m.screen.DrawObject((m.screenSize.width)-((m.screenSize.width) * 0.39),15,canvas)
    m.screen.SwapBuffers()          
    sleep(3000)
end function

function playVideo(fetchedUrl as String)
    while true
        sec = CreateObject("roRegistrySection", "GenSettings")
        m.showTimer = 0
        inputFromDevice = CreateObject("roInput")
        m.logoOnce = true
        m.showTimerMax = 10
        m.chnlCount2=m.totalChannels.data.Count()-1
        mode = 1
        timer = CreateObject("roTimespan")
        fonts = CreateObject("roFontRegistry")
        fonts.Register("pkg:/fonts/vSHandprinted.otf")
        font = fonts.GetFont("vSHandprinted", 28, 500, false)
        m.screen = CreateObject("roScreen")
        m.player = CreateObject("roVideoPlayer")
        port = CreateObject("roMessagePort")
        m.player.setMessagePort(port)
        inputFromDevice.SetMessagePort(port)
        m.showLoadingMsg = true
        m.selectedVideoUrl = fetchedUrl
        m.paused = false
        if m.isRecording = 0
           
            getLastPlayed = sec.Read("lastPlayed")
            sec.Write("secondLastPlayed", getLastPlayed)
            sec.Write("lastPlayed", fetchedUrl)
          
            SVID = m.totalChannels.data[m.selectedChannel].svChannelId.toStr()
        
            getLastSVID = sec.Read("lastSVID")
            sec.Write("secondLastSVID", getLastSVID)
            sec.Write("lastSVID", SVID)

            getLastSelectedChannel = sec.Read("lastSelectedChannel")
            sec.Write("secondLastSelectedChannel", getLastSelectedChannel)                        
            sec.Write("lastSelectedChannel", m.selectedChannel.ToStr())

            getLastPlayedMsg = sec.Read("lastPlayedMsg")
            print getLastPlayedMsg;
            sec.Write("secondLastPlayedMsg", getLastPlayedMsg)
            sec.Write("lastPlayedMsg", m.loadingMsg)
            print "##LAST PLAYED UPDATED##"
        end if
        m.screenSize.width = m.screen.GetWidth()
        m.screenSize.height = m.screen.GetHeight()
        m.screen.setAlphaEnable(true)
        m.screen.SetMessagePort(port)
        m.screen.setAlphaEnable(true)
        m.loadingBitmap = CreateObject("roBitmap", {width:1280, height:720, AlphaEnable: true})
        m.loadingBitmap.DrawRect(260, 650, 765, 8, &hCCCCCCFF)
        m.showLogo = true
        m.playButton=CreateObject("roBitmap","pkg:/images/play.png")
        m.pauseButton=CreateObject("roBitmap","pkg:/images/pause.png")
        m.fastForwardButton=CreateObject("roBitmap","pkg:/images/fastforward.png")
        m.fastRewindButton=CreateObject("roBitmap","pkg:/images/fastrewind.png")    
        m.chnlCount2=m.totalChannels.data.Count()-1    
        m.player.SetContentList([
          {
              Stream : { url :fetchedUrl }
              StreamFormat : "hls"
              TrackIDAudio: "audio_eng"
              TrackIDSubtitle: "eia608/1"
          }
        ])
        
        
        deviceInfo = CreateObject("roDeviceInfo")
        captionsMode = deviceInfo.GetCaptionsMode()
        captions = m.player.GetCaptionRenderer()
        if (mode = 1)
          captions.SetScreen(m.screen)
        endif
        captions.SetMode(mode)
        captions.SetMessagePort(port)
        if (captionsMode = "On")
            captions.ShowSubtitle(true)        
        else if (captionsMode = "Off")
            captions.ShowSubtitle(false)        
        else if (captionsMode = "Instant Replay")
            captions.ShowSubtitle(true)        
        endif
        clearEPG()    
        m.player.play()   
        m.channelFetched = true
        while true
            msg = wait(0, port)
            if m.recordingStarted = true then
                m.showLogo = false

            end if
            if type(msg) = "roVideoPlayerEvent" then
               
                if msg.isStatusMessage() then
                    if msg.GetMessage() = "start of play"
                        m.screen.Clear(&h00000000)
                        m.screen.SwapBuffers()
                        m.bufferProgress = 100
                        m.showLoadingMsg = false     

                        if m.showLogo = true then                      
                            channelInfo()
                            m.showLogo = false
                        end if                   

                        m.initialLoad=false
                     
                        print "################ Video Starts Playing ################"
                        m.epgFlag=true

                    else if msg.GetMessage() = "startup progress"
                        bufferingProgres% = msg.GetIndex() / 10
                        m.bufferProgress = bufferingProgres%
                        print m.bufferProgress
                        m.drawBufferProgressBar()
                    end if
                else if msg.isPlaybackPosition()
                    m.elapsedSeconds = msg.GetIndex()
                    if m.drawSettings.showInfo then
                        m.showTimer = m.showTimer + 1
                        if m.showTimer > m.showTimerMax then
                            m.drawSettings.showInfo = false
                            m.screen.Clear(&h00000000)
                            m.screen.SwapBuffers()
                            m.showTimer = 0
                        else 
                            m.screen.Clear(&h00000000)
                            m.screen.DrawObject(0, 0, m.timeCanvas)
                            m.screen.SwapBuffers()
                        end if
                    end if

                else if msg.isPaused() 
                    print "################ Video Paused ################"

                else if msg.isResumed()
                    print "################ Video Resumed ################"

                else if msg.isRequestFailed() then
                    print "################ Playback Failed ################" + msg.GetMessage()
                        if msg.GetMessage() <> "" then                            
                            getSecondLastPlayed = sec.Read("secondLastPlayed")
                            lastPlayed = sec.Read("lastPlayed")
                            getSecondLastSVID = sec.Read("secondLastSVID")
                            lastSVID = sec.Read("lastSVID")
                            error_playback_failed = "Playback Failure"
                            
                            if getSecondLastPlayed = lastPlayed then
                                clearEPG()
                                errorTop(error_playback_failed)
                                showEPG()
                            else if getSecondLastPlayed = ""
                                clearEPG()
                                errorTop(error_playback_failed)
                                showEPG()
                            else if getSecondLastSVID = lastSVID
                                clearEPG()
                                errorTop(error_playback_failed)
                                showEPG()
                            else if getSecondLastSVID = ""
                                clearEPG()
                                errorTop(error_playback_failed)
                                showEPG()
                            else if m.isRecording = 1
                                clearEPG()
                                errorTop(error_playback_failed)
                                showEPG()
                            else
                                sec.Write("lastPlayed", getSecondLastPlayed)

                                getSecondLastSelectedChannel = sec.Read("secondLastSelectedChannel")
                                sec.Write("lastSelectedChannel", getSecondLastSelectedChannel)

                                getSecondLastPlayedMsg = sec.Read("secondLastPlayedMsg")
                                sec.Write("lastPlayedMsg", getSecondLastPlayedMsg)                             
                                

                                getSecondLastSVID = sec.Read("secondLastSVID")
                                sec.Write("lastSVID", getSecondLastSVID)

                                m.loadingMsg=getSecondLastPlayedMsg
                                clearEPG()

                                

                                getFirstStream(m.totalChannels, error_playback_failed)                                     

                                if m.selectedChannel = m.totalChannels.data.Count()-1
                                    m.selectedChannel=0
                                else
                                    m.selectedChannel=m.selectedChannel+1
                                end if
                                m.loadingMsg="Loading "+m.totalChannels.data[m.selectedChannel].description+" ..."
                                fetchedUrl=getStreamUrl(m.totalChannels.data[m.selectedChannel].svChannelId.toStr())
                                if m.initialLoad = false then
                                else
                                    print "first stream Error"
                                end if
                                'exit while
                            end if
                        end if

                else if msg.isFullResult() then
                    print "################ Video Ended ################"

                else if msg.isPartialResult() then
                    print "################ Video Aborted ################"
                    exit while


                end if
            end if

            if type(msg) = "roCaptionRendererEvent"


                if m.bufferProgress < 100 and m.showLoadingMsg = true then 

                else if m.epgFlag=false then 

                else if msg.isCaptionText()
                    print "isCaptionText"
                    if msg.GetMessage() <> invalid and msg.GetMessage() <> ""
                    DrawCaptionString(m.screen, m.screenSize, msg.GetMessage(), font)
                    timer.Mark()

                else if timer.TotalSeconds() > 2
                    ClearCaptionString(m.screen)
                endif

                else if msg.isCaptionUpdateRequest()
                    print "isCaptionUpdateRequest()"
                    UpdateCaptions(m.screen, captions)

                end if

            end if

            if type(msg) = "roInputEvent" then                
                inputResponse = msg.GetInfo().op
                response=inputResponse.Tokenize("@")
                clearEPG()
                print "### INPUT: "inputResponse
                m.recordingStarted = true
                if response[0] = "recording"

                    m.isRecording = 1
                    m.initialLoad = false                    

                    cmsURL = sec.Read("cmsURL")
                    customerID = sec.Read("CustomerID")

                    dvrAuthenticationResult=dvrAuthentication(cmsURL, customerID, m.deviceID, response[2])                    

                    if dvrAuthenticationResult <> invalid
                        if dvrAuthenticationResult.success = false then
                            errorMessage(dvrAuthenticationResult.message, dvrAuthenticationResult.error_code, true)
                        else                            
                            playUrl = dvrAuthenticationResult.data.event_play_url
                            dvr_title = dvrAuthenticationResult.data.title                            
                            
                            if dvr_title <> invalid
                                if dvr_title <> ""
                                    m.loadingMsg="Loading "+dvr_title  
                                else
                                    m.loadingMsg="Loading Recorded Episode"
                                end if
                            else
                                m.loadingMsg="Loading Recorded Episode"
                            end if                            
                            
                            m.recording_title = m.loadingMsg
                            
                            fetchedUrl= playUrl                            
                            print "### Recording Play URL: " playUrl
                        end if
                    else
                        errorMessage(m.errorCMSMsg, m.errorCMSCode, true)
                    end if
                    exit while
                else
                    if m.channelFetched = true then
                        fetchedUrl=getStreamUrl(response[0])
                        if fetchedUrl <> ""                        
                            print "###############" response[0]
                            for ax=0 to m.totalChannels.data.Count()-1 step 1
                                if(response[0]=m.totalChannels.data[ax].svChannelId.toStr()) then
                                    m.loadingMsg="Loading "+m.totalChannels.data[ax].description+" ..."
                                    m.selectedChannel=ax
                                    m.tempSelectedChannel = m.selectedChannel
                                    m.recordingStarted = false                                                                        
                                end if
                            end for
                        end if
                    exit while
                    end if
                endif
            end If

            if type(msg) = "roUniversalControlEvent" then
                key = msg.getInt()
                if key = 5 then                    
                    ShowMenu()

                else if key = 4 then 
                    if m.epgFlag = true then
                        m.screen.Clear(&h00000000)
                        showEPG()

                    else if m.epgFlag = false then
                         clearEPG()
                    end if


                else if key = 2 then                
                    if m.epgUpDownFlag=true then
                        m.screen.Clear(&h00000000)
                        m.isRecording = 0                    
                        m.tempSelectedChannel= m.tempSelectedChannel-1
                        if(m.tempSelectedChannel<0) then
                            m.tempSelectedChannel=m.chnlCount2
                        end if                       

                        displayChalsOnEpg()
                    else if m.epgUpDownFlag=false then
                        if m.recordingStarted = false
                            if m.channelFetched = true then

                                m.selectedChannel=m.selectedChannel-1
                                if(m.selectedChannel<0) then
                                    m.selectedChannel=m.chnlCount2
                                end if
                                
                                while checkServiceRights(m.totalChannels.data[m.selectedChannel].subscription_type) <> 1                                
                                    m.selectedChannel=m.selectedChannel-1
                                    if(m.selectedChannel <= 0) then
                                        m.selectedChannel=m.chnlCount2
                                    end if
                                end while                               

                                m.isRecording = 0
                                m.tempSelectedChannel = m.selectedChannel
                                m.loadingMsg="Loading "+m.totalChannels.data[m.selectedChannel].description+" ..."
                                fetchedUrl=getStreamUrl(m.totalChannels.data[m.selectedChannel].svChannelId.toStr())
                                while fetchedUrl = ""
                                    fetchedUrl = updateToNextChannel()
                                end while
                                if m.playVideoCalled > 20 then
                                    m.playVideoCalled = m.playVideoCalled - 1

                                else
                                     exit while
                                end if
                            end if                        
                        end if
                    end if                

                else if key = 3 then                
                    if m.epgUpDownFlag=true then
                            m.screen.Clear(&h00000000)
                            m.isRecording = 0                        
                            m.tempSelectedChannel = m.tempSelectedChannel+1
                            if m.tempSelectedChannel > m.chnlCount2 then
                                m.tempSelectedChannel = 0
                            end if                             
                          
                            displayChalsOnEpg()
                     else if m.epgUpDownFlag=false then
                        if m.recordingStarted = false
                            if m.channelFetched = true then

                                m.selectedChannel=m.selectedChannel+1

                                if m.selectedChannel > m.totalChannels.data.Count()-1
                                    m.selectedChannel=0
                                end if
                                
                                while checkServiceRights(m.totalChannels.data[m.selectedChannel].subscription_type) <> 1                                
                                    m.selectedChannel=m.selectedChannel+1

                                    if m.selectedChannel >= m.totalChannels.data.Count()-1
                                        m.selectedChannel=0
                                    end if
                                end while

                                m.isRecording = 0
                                m.tempSelectedChannel = m.selectedChannel
                                m.loadingMsg="Loading "+m.totalChannels.data[m.selectedChannel].description+" ..."
                                fetchedUrl=getStreamUrl(m.totalChannels.data[m.selectedChannel].svChannelId.toStr())
                                while fetchedUrl = ""
                                    fetchedUrl = updateToNextChannel()

                                end while
                                if m.playVideoCalled > 20 then
                                    m.playVideoCalled = m.playVideoCalled - 1

                                else
                                     exit while
                                end if
                            end if                        
                        end if
                    end if                
                else if key = 0 then 
                    m.epgUpDownFlag=false
                    m.exit="NO"
                    exit while

                else if key = 6 then 

                    if m.allowOK=true then
                        m.selectedChannel = m.tempSelectedChannel
                        m.bufferProgress=100
                        m.loadingMsg="Loading "+m.totalChannels.data[m.selectedChannel].description+" ..."
                        m.screen.DrawText(m.loadingMsg, m.loadingMsgX, m.loadingMsgY, &hFFFFFFFF, m.loadingTextFont)

                        m.recordingStarted = false
                        clearEPG()  


                        fetchedUrl=getStreamUrl(m.totalChannels.data[m.selectedChannel].svChannelId.toStr())
                        
                    end if
                    while fetchedUrl = ""
                        fetchedUrl = updateToNextChannel()
                       
                    end while
                    if fetchedUrl <> ""
                        exit while
                    else
                        
                    end if

                    if m.exit = "NO" then
                        exit while
                    end if

                else if key = 13 then 'play/pause
                    if m.bufferProgress=100 then
                        clearEPG()
                        m.showTimer = 0
                        if m.paused then
                            m.paused = false
                            m.player.Resume()
                            'm.timeCanvas.DrawObject(145,615,m.pauseButton)
                            'm.screen.DrawObject(0, 0, m.timeCanvas)
                            m.screen.SwapBuffers()
                            sleep(800)
                            m.drawSettings.showInfo = false
                            m.screen.Clear(&h00000000)
                            m.screen.SwapBuffers()
                        else
                            m.paused = true
                            m.player.Pause()
                            m.drawSettings.showInfo = true
                            m.screen.Clear(&h00000000)
                            'm.timeCanvas.DrawObject(145,615,m.playButton)
                            'm.screen.DrawObject(0, 0, m.timeCanvas)
                            m.screen.SwapBuffers()
                        end if


                    end if
                end if
            end if

        end while
        if m.exit <> "NO" then
            if m.initialLoad = false then
                if fetchedUrl <> "" then   
                    m.channelFetched = false                   
                    m.screen = invalid
                end if
            else                    
                getFirstStream(m.totalChannels)
                exit while
            end if
        end if
    end while
End Function


Function updateToNextChannel()
     if m.selectedChannel >= m.totalChannels.data.Count()-1
        m.selectedChannel=0
    else
        m.selectedChannel=m.selectedChannel+1
    end if
    m.loadingMsg="Loading "+m.totalChannels.data[m.selectedChannel].description+" ..."
   
    url=getStreamUrl(m.totalChannels.data[m.selectedChannel].svChannelId.toStr())
    return url
End Function

Function dvrAuthentication(cmsURL, customerID, deviceID, eventID)
    dvrAuthenticationURL = cmsURL+"api/dvr/"+customerID+"/"+deviceID+"/"+eventID+"/authorize"    
    dvrAuthenticationObject = CreateApiURLTransferObject(dvrAuthenticationURL)
    dvrAuthenticationResponse = dvrAuthenticationObject.getToString()
    response=ParseJson(dvrAuthenticationResponse)
    return response
End Function  

Function UpdateCaptions(screen as object, captions as object) As Void
    screen.Clear(&h00)
    captions.UpdateCaption()
    screen.SwapBuffers()
End Function
  
Function DrawCaptionString(screen as object, screenSize as object, caption as String, font as object) as Void
    screen.Clear(&h00)
    textHeight = font.GetOneLineHeight()
    textWidth = font.GetOneLineWidth(caption, screenSize.width)
    screen.DrawText(caption, 300, 300, &hd5d522ff, font)
    screen.SwapBuffers()
End Function
  
Function ClearCaptionString(screen as object) As void
    screen.Clear(&h00)
    screen.SwapBuffers()
End Function
  
function getStreamUrl(svChannelId, recording=0, recordingURL="")
    if recording = 1
        returnUrl = recordingURL
    else        
        m.channelFetched = false
        sec = CreateObject("roRegistrySection", "GenSettings")
        cmsURL = sec.Read("cmsURL")
        url = cmsURL+"api/customer/"+m.deviceID+"/authorize"       
        streamURLObject = HttpUtils(url)       
        streamURLObject.AddParam("channelID", svChannelId)
        jsonResponse = streamURLObject.PostFromStringWithRetry("")
        jsonData=ParseJson(jsonResponse) 
                
        if jsonData <> invalid
            returnUrl=CreateObject("roString")
            if jsonData.success=true then
                returnUrl=jsonData.data.url
            else
                description = ""
                for counter=m.initalLoadIndex to m.totalChannels.data.count()-1 step 1                    
                    if m.totalChannels.data[counter].svChannelId = svChannelId.toInt()
                        description = m.totalChannels.data[counter].description
                    end if
                end for
                errorMessage(jsonData.message, jsonData.error_code, true, description)
            end if
        else        
           errorMessage(m.errorCMSMsg, m.errorCMSCode, true)
        end if        
        m.channelFetched = true
    endif
    return returnUrl
end function

function ShowMenu() As Void
     
    generalSettings = CreateObject("roRegistrySection", "GenSettings")    
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
    dialog.SetTitle("StreamVision Menu")
    dialog.SetText("Service Provider ID: "+generalSettings.Read("ServiceProviderID"))
    dialog.SetText("Customer ID: "+generalSettings.Read("CustomerID"))
    dialog.SetText("Version: 1.1")
    dialog.SetText("Build: 1.1.10.17")
    dialog.addButton(1, "Change Account Settings")
    dialog.addButton(2, "Purge Account Settings")
    dialog.addButton(3, "Exit")
    dialog.EnableBackButton(false)
    dialog.Show()
    While True
        dlgMsg = wait(0, dialog.GetMessagePort())
        If type(dlgMsg) = "roMessageDialogEvent"
            if (dlgMsg.isButtonPressed()) then
                if (dlgMsg.GetIndex() = 1) then
                    m.player.pause()
                    dialog.Close()
                    addServiceProviderIDScreen()
                else 
                    if (dlgMsg.GetIndex() = 2) then
                        generalSettings.Delete("ServiceProviderID")
                        generalSettings.Delete("cmsURL")
                        generalSettings.Delete("heading")
                        generalSettings.Delete("logo")
                        generalSettings.Delete("CustomerID")
                        generalSettings.Delete("Password")                        
                        generalSettings.Delete("secondLastPlayed")
                        generalSettings.Delete("lastPlayed")
                        generalSettings.Delete("secondLastSVID")
                        generalSettings.Delete("lastSVID")
                        generalSettings.Delete("secondLastSelectedChannel")
                        generalSettings.Delete("lastSelectedChannel")
                        generalSettings.Delete("secondLastPlayedMsg")
                        generalSettings.Delete("lastPlayedMsg")        
                        dialog.Close()
                        addServiceProviderIDScreen()
                    else if (dlgMsg.GetIndex() = 3) then                    
                        dialog.Close()
                        if m.recordingStarted = true then
                            m.loadingMsg = "Loading "+m.recording_title
                            playVideo(m.selectedVideoURL)
                        else
                            m.recordingStarted = false
                            getFirstStream(m.totalChannels)
                        end if
                    end if
                   
                end if
            else if (dlgMsg.isScreenClosed()) then                
                exit while                
            else
                exit while                
            end if
        end if
    end while 
End function

function showEPG()
        m.tempSelectedChannel = m.selectedChannel
	displayChalsOnEpg()
	m.epgFlag=false
        m.allowOK=true
        m.epgUpDownFlag=true    
end function

function displayChalsOnEpg()
        canvas=CreateObject("roBitmap", {width:400, height:720, AlphaEnable: true})
	canvas.Clear(&h404040A0)
	for z=0 to 7 step 1
            canvas.drawRect(m.chnlDivLineX,m.chnlDivLineY,m.chnlDivLineWidth,m.chnlDivLineHeight,&h000000FF)
            m.chnlDivLineY=m.chnlDivLineY+80
	end for
	'selected box black
	canvas.drawRect(m.chnlSelctBoxX,m.chnlSelctBoxY,m.chnlSelctBoxWidth,m.chnlSelctBoxHeight,&h54bf26A0)
	'selected box green
	canvas.drawRect(m.chnlSelctBoxInnerX,m.chnlSelctBoxInnerY,m.chnlSelctBoxInnerWidth,m.chnlSelctBoxInnerHeight,&h000000A0)
	counter1=0
	counter2=m.totalChannels.data.Count()
	
	' print "------------------"; counter2
	counterXY=0
	drwTxt1X=140
	drwTxt1Y=10
	drwTxt2X=140
	drwTxt2Y=30
	drwTxt3X=140
	drwTxt3Y=45
	' print "------------------------"; m.tempSelectedChannel
	for x=m.tempSelectedChannel-4 to m.tempSelectedChannel+4 step 1
		if x>m.totalChannels.data.Count()-1 then
			canvas.DrawScaledObject(m.epgChnlImageX,m.epgChnlImageY,0.9,0.9,m.channelLogoRepo[counter1])
			canvas.DrawText("Now :",drwTxt1X,drwTxt1Y,&h00BFFFFF,m.loadingFont)
			drwTxt1Y=drwTxt1Y+80 
			if toStr(m.totalChannels.data[counter1].channelId) <> "" then
                            for y=0 to m.nowShowResponse.data.Count()-1 step 1
                                    if toStr(m.totalChannels.data[counter1].channelId) = toStr(m.nowShowResponse.data[y].channelId) then
                                            canvas.DrawText(m.nowShowResponse.data[y].Title,drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
                                            exit for
                                    end if
                            end for
			else
				canvas.DrawText("",drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
			end if
			drwTxt2Y=drwTxt2Y+80
			counter1=counter1+1
			if counter1=9 then
				m.tempSelectedChannel=4
				m.chnlCount=0
			end if
		else if x<0
			counterXY=counterXY+1
			if counterXY=9 then
				m.tempSelectedChannel=4
				m.chnlCount2=m.totalChannels.data.Count()-1
			end if
			canvas.DrawScaledObject(m.epgChnlImageX,m.epgChnlImageY,0.9,0.9,m.channelLogoRepo[counter2+x])
			canvas.DrawText("Now :",drwTxt1X,drwTxt1Y,&h00BFFFFF,m.loadingFont)
			drwTxt1Y=drwTxt1Y+80
			' print "SSSSSSSSSSSSSSSSSS :"; counter2+x
			if toStr(m.totalChannels.data[counter2+x].channelId) <> "" then
				for y=0 to m.nowShowResponse.data.Count()-1 step 1
					if toStr(m.totalChannels.data[counter2+x].channelId) = toStr(m.nowShowResponse.data[y].channelId) then
						canvas.DrawText(m.nowShowResponse.data[y].Title,drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
						exit for
					end if
				end for
			else
				canvas.DrawText("",drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
			end if
			drwTxt2Y=drwTxt2Y+80
		else 
			canvas.DrawScaledObject(m.epgChnlImageX,m.epgChnlImageY,0.9,0.9,m.channelLogoRepo[x])
			canvas.DrawText("Now :",drwTxt1X,drwTxt1Y,&h00BFFFFF,m.loadingFont)
			drwTxt1Y=drwTxt1Y+80
			if toStr(m.totalChannels.data[x].channelId) <> "" then
				for y=0 to m.nowShowResponse.data.Count()-1 step 1
					if toStr(m.totalChannels.data[x].channelId) = toStr(m.nowShowResponse.data[y].channelId) then
						canvas.DrawText(m.nowShowResponse.data[y].Title,drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
						exit for
					end if
				end for
			else
				canvas.DrawText("",drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
			end if
			drwTxt2Y=drwTxt2Y+80
		end if 
		m.epgChnlImageY=m.epgChnlImageY+80
	end for
	counter1=0
	counter2x=6
	counter2y=8
	m.screen.DrawObject(0,0,canvas)
	m.screen.SwapBuffers()
	m.chnlDivLineX=0
	m.chnlDivLineY=78
	m.epgChnlImageX=35
	m.epgChnlImageY=8
end function

function clearEPG()
	m.screen.Clear(&h00000000)
	m.screen.SwapBuffers()
	m.screen.Clear(&h00000000)
	m.epgFlag=true        
	m.allowOK=false
        m.epgUpDownFlag=false
	if m.bufferProgress<100
            m.drawBufferProgressBar()
        end if
end function

function getCurrentShow()
  sec = CreateObject("roRegistrySection", "GenSettings")
    cmsURL = sec.Read("cmsURL")
    nowShow = CreateObject("roUrlTransfer")    
    nowShow.SetURL(cmsURL+"api/epg/channels/currentShows")
    showResponse = ParseJson(nowShow.GetToString())
    
    if showResponse <> invalid then
        if showResponse.success=true then
            m.nowShowResponse = showResponse
            
        else
            errorMessage(showResponse.message, showResponse.error_code, true)
        end if
    else
        errorMessage(m.errorCMSMsg, m.errorCMSCode, true)
    end if
end function

sub videoScreenDrawProgressBar()
 duration = 1
    m.timeCanvas = CreateObject("roBitmap", {width:1280, height:720, AlphaEnable: true})
	m.timeCanvas.Clear(&h00000000)
	time = m.timeFromSeconds(m.elapsedSeconds)
    m.timeCanvas.DrawText(time, 70, 623, &h7A7A7AFF, m.loadingFont)
    'progress bar underlay
    m.timeCanvas.DrawRect(195, 632, 890, 8, &h7B7B7B90)

	if duration <> 0
    	progress = m.elapsedSeconds / duration
    	progressWidth = progress * 890
    	'draw progress
    	m.timeCanvas.DrawRect(195, 632, progressWidth, 8, &h00BFFFFF)

    	secondsRemainding = duration - m.elapsedSeconds
		time = m.timeFromSeconds(secondsRemainding)

    	'time left label
		m.timeCanvas.DrawText(time, 1100, 623, &h7A7A7AFF, m.loadingFont)
    end if

	m.screen.DrawObject(0, 0, m.timeCanvas)
end sub


function videoScreenSecondsFromTime(time as String) as Integer
    return time.toInt()
end function

function videoScreenTimeFromSeconds(seconds as Integer) as String
	
	hours = Int(seconds / 3600)
	if hours < 1
		minutes = Int(seconds / 60)
		seconds = seconds MOD 60
	else 
		minutes = Int(seconds / 60)
		minutes = minutes - (hours*60)
		seconds = seconds - (hours*3600) - (minutes*60)
	end if

	if hours < 10 then
		hourStr = "0" + hours.toStr()
	else
		hourStr = hours.toStr()
	end if

	if minutes < 10 then
		minuteStr = "0" + minutes.toStr()
	else
		minuteStr = minutes.toStr()
	end if
	if seconds < 10 then
		secondStr = "0" + seconds.toStr()
	else
		secondStr = seconds.toStr()
	end if

	return hourStr + ":" + minuteStr + ":" + secondStr
end function

Function checkServiceRights(channelSubscription)
    access = 0

    for serviceCounter = 0 to m.customerBaseServices.Count()-1
        if channelSubscription = m.customerBaseServices[serviceCounter]
            access = 1
            return access
        end if
    end for
 
    return access
End Function

sub drawVideoBufferProgressBar()
    if m.bufferProgress < 100 then   
        progress = m.bufferProgress / 100
        progressWidth = progress * 765
        m.loadingBitmap.Clear(&h00000000)
        m.screen.SwapBuffers()
        m.screen.Clear(&h00000000)
        m.loadingBitmap.DrawRect(260, 650, 765, 8, &hCCCCCCFF)    
        m.loadingBitmap.DrawRect(260, 650, progressWidth, 8, &h2E2EFEFF)
        m.loadingMsgX=(m.deviceInfoData.w-(len(m.loadingMsg)*20))\2
        if m.showLoadingMsg = false then
            m.loadingMsg = ""       
        end if
    
        m.loadingMsgY = m.screenSize.height * 0.4
        m.screen.DrawText(m.loadingMsg, m.loadingMsgX, m.loadingMsgY, &hFFFFFFFF, m.loadingTextFont)
        m.loadingBitmap.DrawText(m.bufferProgress.toStr() + "%" , 950, 620, &hFFFFFFFF, m.loadingPercentFont)
        m.screen.DrawObject(0, 0, m.loadingBitmap)
        m.screen.SwapBuffers()
   
    end if
        
end sub