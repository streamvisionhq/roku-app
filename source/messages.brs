Function errorMessage(message, error_code, showProfile, description="") As Void
    sec = CreateObject("roRegistrySection", "GenSettings")
    showExitButton = true
    exitButtonIndex = 1
    showOkButton = false
    closeAPP = false
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
    dialog.SetTitle("ERROR")
    dialog.SetText(error_code+" - "+message)

    if error_code = "2007" then        
        showProfile = false
        if description <> ""
            dialog.SetText("Channel: "+description)
        end if        
        dialog.AddButton(exitButtonIndex, "OK")
        showExitButton = false        
    end if
    
   if error_code = "2019" then      
        showOkButton = true
        showExitButton = false
        
    end if

    if showProfile = true then 
        dialog.AddButtonSeparator()
        dialog.AddButton(1, "Change Account Settings")
        exitButtonIndex = exitButtonIndex + 1
       
    end if
    if showExitButton = true then
        dialog.AddButton(exitButtonIndex, "Exit")
    end if

    if showOkButton = true then
        dialog.AddButton(exitButtonIndex, "OK")
    end if
     
    dialog.EnableBackButton(false)
    dialog.Show()
    
    if error_code = "2001" then        
        While True
            dlgMsg = wait(0, dialog.GetMessagePort())
            if type(dlgMsg) = "roMessageDialogEvent"
                if dlgMsg.isButtonPressed()
                    if (dlgMsg.GetIndex() = exitButtonIndex) then
                        m.exit="YES"
                        m.exitDelete = true                        
                        End
                        exit while
                    end if
                end if
            end if
        end while
    else
        While True
            dlgMsg = wait(0, dialog.GetMessagePort())
            If type(dlgMsg) = "roMessageDialogEvent"
                if dlgMsg.isButtonPressed()
                    if (dlgMsg.GetIndex() = exitButtonIndex) then                                          
                        if showExitButton = true OR showOkButton = true
                            m.exit="NO"
                            exit while
                        else                        
                            lastChannelID=sec.Read("lastSelectedChannel").ToInt()                             
                            if sec.Read("lastPlayed") <> "" then
                                getFirstStream(m.totalChannels)
                            else
                                if m.selectedChannel = m.totalChannels.data.Count()-1
                                    m.selectedChannel=0
                                else
                                    m.selectedChannel=m.selectedChannel+1
                                    m.loadingMsg="Loading "+m.totalChannels.data[m.selectedChannel].description+" ..."
                                    fetchedUrl=getStreamUrl(m.totalChannels.data[m.selectedChannel].svChannelId.toStr())
                                    playVideo(fetchedUrl)
                                end if
                            end if                        
                        end if                    
                    else if (dlgMsg.GetIndex() = 1) AND showProfile = true then                    
                        addServiceProviderIDScreen()
                        dialog.Close()
                    end if
                else if dlgMsg.isScreenClosed()
                    m.exit="NO"              
                    exit while
                endif
            end if        
        end while
    end if
End Function

Function blankFieldMessages(message) As Void
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
   
    dialog.SetText(message)
    dialog.AddButton(1, "OK")
    dialog.EnableBackButton(false)
    dialog.Show()
    While True
        dlgMsg = wait(0, dialog.GetMessagePort())
        If type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                m.exit="NO"
                exit while
            else if dlgMsg.isScreenClosed()
                m.exit="NO"
                exit while
            endif
        end if        
    end while 
End Function